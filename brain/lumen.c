#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
/**
 * \file lumen.c
 * \author Dorival.A
 *
 * \brief Affiche le nombre de case dans le noir.
 *
 * */

/**
 *
 * \brief Fonction qui mes les valeurs de luminosité.
 *
 * Cette fonction est une fonction recursive qui mes les
 * bonne valeurs de luminosité dans les cases. Les conditions
 * d'arret sont quand le niveau de lumiere est déjà plus
 * élever que se qu'on veux mettre la oú on est, ou alors
 * quand le niveau de lumiere qu'on veux mettre est de 0.
 *
 * \return void
 *
 * */
void light(char grid[][25], int n, int lvl, int x, int y)
{
    if ((grid[x][y] >= lvl + '0' && grid[x][y] <= '9' )|| lvl == 0)
    {
        return ;
    }
    int i, j;
    for (i = -1; i < 2; ++i)
    {
        for ( j = -1; j < 2; ++j )
        {
            if ( x + i < n  && y+j < n && x+i >= 0 && y+j >= 0)
            {
            light(grid, n, lvl - 1, x + i, y + j);
            }
        }
    }
    if (grid[x][y] != 'C')
    {
        grid[x][y] = lvl + '0';
    }
    return;
}

/**
 * \brief Compte le nombre de 0.
 *
 * La fonction compte le nombre de caractère '0' dans
 * un tableau 2D.
 *
 * \return nombre de '0'
 * */
int count0(char grid[][25], int n)
{
    int i, j;
    int count = 0;
    for ( i = 0; i < n; ++i )
    {
        for ( j = 0; j < n; ++j )
        {
            if ( grid[i][j] == '0' )
            {
                ++count;
            }
        }
    }
    return count;
}

/**
 *
 * \brief Fonction principale
 *
 * La fonction prend en input le tableau et les valeurs associer.
 * Ensuite on applique la fonction light a chaque fois qu'on passe
 * sur un 'C' car ils correspondent à des points de lumiere.
 * */
int main()
{
    int N;
    scanf("%d", &N);
    int L;
    scanf("%d", &L);
    char grid[25][25];
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            char tmp[4];
            scanf("%s", tmp);
            if (strcmp(tmp, "X") == 0)
            {
                grid[i][j] = '0';
            }else {
                grid[i][j] = 'C';
            }
        }
    }
    for ( int i = 0; i < N; ++i )
    {
        for ( int j = 0; j < N; ++j )
        {
            if( grid[i][j] == 'C' )
            {
                light(grid, N, L, i, j);
            }
        }
    }


    // Write an answer using printf(). DON'T FORGET THE TRAILING \n
    // To debug: fprintf(stderr, "Debug messages...\n");

    printf("%d\n", count0(grid, N));

    return 0;
}
