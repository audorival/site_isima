#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
/**
 * \file brain.c
 * \author Dorival.A
 * \brief Compilateur Brainfuck
 * */

/**
 *
 * \brief Initialise un tableau de caractère
 *
 * La fonction permet de mettre toute les valeurs d'un
 * tableau carré de caractère à 'a'.
 * */
void initTab(char tab[][1025], int line)
{
    int i, j;
    for ( i = 0; i < line; ++i )
    {
        for ( j = 0; j < 1025; ++j )
        {
            tab[i][j] = 'a';
        }
    }
}

/**
 * \fn int nbrToRemove(char prog[][1025], int i, int j)
 * \brief Nombre à retirer à l'oindice pour faire la boucle
 *
 * Cette fonction permet de revenir au debut de la boucle.
 * En effet elle permet de retrouver le debut de la boucle
 * et de donner le nnombre à soustraire a l'indice pour refaire un tour de boucle.
 *
 * \return Le nombre a soustraire a l'indice.
 * 
 * */
int nbrToRemove(char prog[][1025], int i, int j)
{
    j--;
    int verif = -1;
    int nbr_remove = 1;
    while (verif < 0)
    {
        switch (prog[i][j]) {
            case '[':
                ++verif;
                break;
            case ']':
                --verif;
        }
        --j;
        if (j == - 1)
        {
            j = 150;
            --i;
        }
        ++nbr_remove;
    }
    return nbr_remove;
}

/**
 * \fn int difOpenClose(char prog[][1025], int line)
 * \brief Difference entre les crochets ouvrant et fermant.
 *
 * La fonction permet de donner la differance entre les crochets ouvrant et fermant,
 * un '[' vaut 1 et ']' vaut -1.
 *
 * \return 0 si le nombre de '[' == nbr de ']', Sinon renvois un nombre > 0 si + de '[' et < 0 si + de ']'.
 *
 * */
int difOpenClose(char prog[][1025], int line)
{
    int i, j;
    int count = 0;
    for ( i = 0; i < line; ++i )
    {
        for ( j = 0; j < 1025; ++j )
        {
            switch (prog[i][j]) {
                case '[':
                    ++count;
                    break;
                case ']':
                    --count;
                    break;
            }
        }
    }
    return count;
}

/**
 * \fn int main(void)
 * \brief La fonction principale.
 *
 * La fonction main créer tous se qu'on a besoin pour interpreter
 * du code en Brainfuck. Il y a la recuperration des different input
 * donner dans l'enoncée de l'exercice, il y a aussi le tableau sur
 * qui le Brainfuck vas influencer ainsi que l'indice oú on se situe.
 *
 * \return 0 si tout se passe bien, 1 si il y a une erreur dans le code Brainfuck
 *
 *
 * */
int main()
{
    char INVAL[18] = "INCORRECT VALUE";
    char POBO[30] = "POINTER OUT OF BOUNDS";
    int L;
    int S;
    int N;
    int input[10];
    int ii = 0;
    char program[100][1025];
    initTab(program, 100);
    char output[1025] = "";
    scanf("%d%d%d", &L, &S, &N); fgetc(stdin);
    for (int i = 0; i < L; i++) {
        scanf("%[^\n]", program[i]); fgetc(stdin);
    }
    for (int i = 0; i < N; i++) {
        scanf("%d", &input[i]);
    }
    if (difOpenClose(program,L) != 0)
    {
        puts("SYNTAX ERROR");
        return 1;
    }
    int i, j;
    int index = 0;
    int *tab = malloc(sizeof(*tab) * S);
    char tmp[20];
    for (i = 0; i < L; ++i){
        for (j = 0; j < 1025; ++j){
            switch (program[i][j]) {
                case '>':
                    ++index;
                    if (index > S-1)
                    {
                        printf("%s\n", POBO);
                        return 1;
                    }
                    break;
                case '<':
                    --index;
                    if ( index < 0 )
                    {
                        printf("%s\n", POBO);
                        return 1;
                    }
                    break;
                case '-':
                    --tab[index];
                    if (tab[index] < 0)
                    {
                        printf("%s\n", INVAL);
                        return 1;
                    }
                    break;
                case '+':
                    ++tab[index];
                    if ( tab[index] > 255 )
                    {
                        printf("%s\n", INVAL);
                        return 1;
                    }
                    break;
                case '.':
                    sprintf(tmp, "%c", tab[index]);
                    strcat(output, tmp);
                    break;
                case ',':
                    tab[index] = input[ii];
                    ++ii;
                    break;
                case ']':
                    if (tab[index] != 0)
                    {
                        j -= nbrToRemove(program, i, j);
                    }
            }


        }
    }


    // Write an answer using printf(). DON'T FORGET THE TRAILING \n
    // To debug: fprintf(stderr, "Debug messages...\n");

    printf("%s\n", output);

    return 0;
}
