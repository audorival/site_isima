# Résolution Ghost legs

Aubin Dorival Prép'isima 1 2023/2024

![UCA logo](https://gitlab.isima.fr/audorival/markdownfichiers/-/raw/master/UCA.jpg)  

![River](https://gitlab.isima.fr/audorival/markdownfichiers/-/raw/master/logo_river.jpg)


## Le problème

### Énoncé

>Une rivière numérique est une séquence de nombres où chaque nombre est suivi par le même nombre plus la somme de ses chiffres. Dans une telle séquence 123 est suivi de 129 (depuis 1 + 2 + 3 = 6), qui est encore suivi de 141.
>
>Nous appelons une rivière numérique K, si elle commence par la valeur K.
>
>Par exemple, la rivière 7 est la séquence commençant par {7, 14, 19, 29, 40, 44, 52, ... } et la rivière 471 est la séquence commençant par {471, 483, 498, 519, ... }.
>
>Les rivières numériques peuvent se rencontrer. Cela se produit lorsque deux rivières numériques partagent les >mêmes valeurs. La rivière 32 rencontre la rivière 47 à 47, tandis que la rivière 471 rencontre la rivière 480 à 519.
>
>Étant donné que deux rivières numériques de réunion impriment le point de rencontre.

### Le problème 1

Le premier problème est de trouver le point de rencontre dans la suite de riviere entre deux 
nombres d'entrées. Avec 0 < `r1` ≤ 20000000, 0 < `r2` ≤ 20000000, avec `r1` et `r2` les deux entrées.

### Le problème 2

Le deuxieme problème est de savoir si un nombre en entrée (`r1`) peut être le point de rencontre 
de deux nombres diffèrent dans leur suite de la rivière. Avec 1 ≤ `r1` < 100000


## Ma Solution

### Problème 1
  
#### Méthode

Il suffit de trouver un cas où les deux valeurs sont égaux.

#### Algorithme

Ma solution pour le premier problème étais de regarder le quel entre `r1` et `r2` étais le plus petit
, puis le plus petit actuel je le fais avancer dns la rivière jusqu'a ce que `r1` soit égale à `r2`. 

#### Mon code 
```c
// fonction permetant de faire le prochain nombre de la suite de riviere
int ajout (int val){
    int result = 0;
    int temp =0;
    while(val > 0){
        temp = val % 10;
        val = val /10;
        result += temp;
    } 
    return result;
}

int main()
{
    long long r_1;
    scanf("%lld", &r_1);
    long long r_2;
    scanf("%lld", &r_2);

    // Write an answer using printf(). DON'T FORGET THE TRAILING \n
    // To debug: fprintf(stderr, "Debug messages...\n");
    fprintf(stderr, "%d : \n", ajout(42));
    while(r_1 != r_2){ // on s'arrete pas tant que les deux nombre ne sont pas egeaux

        if(r_1 < r_2){ // si r1 est plus petit
            r_1 += ajout(r_1); // on prend le prochain r1
        }else if (r_2 < r_1){ // si r2 est plus petit 
            r_2 += ajout(r_2); // on prend le prichain r2
        }
    }
    printf("%lld\n", r_2); // quand r1 egale r2 alors on affiche l'un des deux pour donner l'endroit de rencontre

    return 0;
}
```

### Problème 2

#### Méthode

Il faut trouver un autre nombre qui permet dans la suite de la rivier est égale à la valeur
d'entrée.

#### Algorithme

Ma logique pour le problème 2 est de considerer `r1` comme l'un des deux nombres qui doit se rejoindre, 
se qui permet de chercher pour un seul autre nombre. Puis pour chercher ce deuxieme nombre, on sait que `r1`
est compris entre 1 pas inclu et 100000 inclu se qui reduit deja notre zone de recherche dans l'interval :
[1, r1[. Donc on prend un nombre égale à 1 puis on fais un essai en faisant la riviere. Si c'est pas égale 
à `r1` alors on prend le nombre suivant et on continue jusqu'à ce que le nombre test dépasse `r1`, on affichera
"NO" , ou alors quand le nombre de test est égale à `r1` alors on affichera "YES".

#### Mon code 
```c
// fonction permetant de faire le prochain nombre de la suite de riviere
 int ajout(int val){
    int res = 0;
    while (val > 0){
        res += val % 10;
        val = val / 10;
    }
    return res;
 }

int main()
{
    
    int r_1;
    scanf("%d", &r_1);
    int bon = 0; // permet de savoir si on a trouvé
    int test = 1; // nombre de test

    // Write an answer using printf(). DON'T FORGET THE TRAILING \n
    // To debug: fprintf(stderr, "Debug messages...\n");
    while (bon == 0 && test < r_1){ // boucle tqnt que test est plus petit que r1 et quand bon n'est pas encore vrai
        int temp = test; // nombre temporaire
        
        temp += ajout(temp);  // nombre suivant de la rivier a partire de test
        
        if (temp == r_1){ // si on a trouvé 
            bon += 1; // on mets bon a vrai pour sortire de la boucle
        }
        test ++; // incrementation de 1 de test pour continuer a tester 
    }
    if (bon >= 1){ // si bon est vrai
        printf("YES"); // affiche yes
    }else{ // si on a pas trouver
        printf("NO"); // affiche no
    }

    return 0;
}
```



## Autre solution 


### Problème 1

#### La logique

La logique d'une autre solution est un peu la même que la mienne mais il utilise la propriaité de pouvoir 
appeler la fonction `main` avec les deux nombre `r1` et `r2` en paramètre. Il a un cas pour recuperer `r1` et
`r2` , un cas quand on a trouvé `r1` = `r2` pour l'affiché, un cas quand `r1` est plus grand, il effectu donc r2 égale a la suite dans la riviere et appelle la fonction main comme une boucle. Il y a aussi l'autre cas ou `r2` égale `r1` cela est le même principe mais inversé. Ca logique me plait beaucoup car cela prend pas beaucoup
de ligne de code.

#### Code
```c
#include <stdio.h>
// --------------------------------------------
int
s(int n) {
    return n ? n % 10 + s( n / 10 ) : 0 ;
}
// --------------------------------------------
main(r1, r2) {
    if ( r1 == 1  ) scanf("%d %d", &r1, &r2);
    if ( r1 == r2 ) printf("%d", r1);
    if ( r1 > r2  ) main ( r1         , r2 + s(r2) );
    if ( r1 < r2  ) main ( r1 + s(r1) , r2         ); 
}
```

### Problème 2

#### La logique

La logique d'une autre solution est de partire de `r1` puis de tester par ordre decroissant en 
soustrayant 1 au nombre de test, cela est plus rapide que ma logique car quand on prend un grand 
`r1` cela peut prendre beaucoup de temps d'arriver au voisinage de `r1`.

#### Code
```c
int nextInRiver(int x){
  int r = x;
  int somme = 0;

  do{
    somme+=r%10;
    r= r/10;}
  while (r!=0);

  x+=somme;
  return x;
}


int main()
{
    int r_1;
    int r_x;
    int next;
    scanf("%d", &r_1);
    r_x = r_1;
    do {
      r_x--;
      next = nextInRiver(r_x);
    }
    while(next != r_1 && r_x>0);
    if (next == r_1)
      printf("YES\n");
    else
      printf("NO\n");

    return 0;
}
```


## Bilan apprentissage

- J'ai apris a optimiser mon code pour le deuxieme problème car j'ai rencontrer des problèmes que ma solution prend trop de temps  
- Premiere utilisation des `long long int`  
- Utilisation de main comme une fonction pour créer une boucle
- Grace a la solution d'un autre pour le problème 2 j'ai vu que mon code pourrais être plus rapide
