Projet d'un jeu clicker
=======================

Principe du jeu
---------------

Le but d' un clicker est de cliquer sur des boutons pour pouvoir gagner
de l'argent virtuel, l'argent permet après d'acheter des améliorations
pour avoir encore plus d'argent à chaque clique sur les boutons. Cela se
répétant à l'infini. Le jeu qui a lancé cette mode est [Cookie
Clicker](https://en.wikipedia.org/wiki/Cookie_Clicker). Si vous voulez
plus d'information sur se genre de jeu il y a un [lien
wikipedia](https://en.wikipedia.org/wiki/Incremental_game) pour cela.

Logo de Cookie Cliker :  
![logo
cookie](https://gitlab.isima.fr/audorival/markdownfichiers/-/raw/master/Cookie_Clicker_logo.png)

Calcule des gain de chaque bouton
---------------------------------

Pour calculer le gain il nous faut appliquer cette formule avec les
differentes variables des boutons:

![Calcule
gain](https://gitlab.isima.fr/audorival/markdownfichiers/-/raw/master/math1.png)

Calcule du prix d'amélioration
------------------------------

Idem pour calculer le prix des améliorations :

![Calcule prix](https://gitlab.isima.fr/audorival/markdownfichiers/-/raw/master/math2.png)

Principe de FuNuMu
------------------

- "Fu" : Fire unit
- "Nu" : Nutritional unit
- "Mu" : Material unit  

Le principe du jeu est de recolter differentes resources qui sont :
des Fu, des Nu et des Mu. L'argent dans se jeux n'existe pas mais se
qui la remplace c'est ces ressources qui permet de tout acheter,
cela est plus difficile car il faut gérer 3 resources à la fois.

Different boutons
-----------------

  Fu        Nu           Mu
  --------- ------------ --------
  Bois      Recolte      Pierre
  Charbon   Champ        Fer
  Gaz       Viande       Cuivre
  Petrol    Artificiel   Bronze

Ce tableau n'est pas representatif des choix finaux car cela pourrait
changer, par exemple en ajoutant d'autre boutons.

To do liste
-----------

- [X] Faire le code des boutons
- [ ] Organiser les boutons dans la zone de jeu
- [ ] Faire fonctionner chaque boutons en assignant leur valeur de départ

Exemple de fonction
-------------------------

Cette fonction  représente un achat d'amélioration, mais elle est pas fini, elle lui manque 
la prise en charge des prix de chaques materiaux :

::: {#cb1 .sourceCode}
```c

public void buy_upgrade(Button button,Button up_button, float fire, float nutritional, float material)
    {
        
        if (fire <=  fire_unit_float)
        {
            fire_unit_float -= fire;
            int up = button.Get("upgrade").AsInt32() + 1;
            button.Set("upgrade", up);
            update_text_button(fire_unit_button, affichage_fire_unit());
            update_text_button(up_button, up.ToString() +"|" + Math.Round( price_fire(button)));
        }
    }
```
:::

Fonction de click sur un bouton pour obtenir des materiaux:

::: {#cb2 .sourceCode}
```c
public void click(Button button)
    {
        float up = button.Get("upgrade").AsInt32();
        float gain = button.Get("gain").AsInt32();
        fire_unit_float += gain * (float)Math.Pow(gain_multi,up) ;
        update_text_button(fire_unit_button, affichage_fire_unit());
    }
```
:::

Conversion HTML
---------------

### Le HTML

[Le html de ce markdown est
ici](https://gitlab.isima.fr/audorival/markdownfichiers/-/raw/master/project.html)

### Le CSS

::: {#cb3 .sourceCode}
``` {.sourceCode .css}
html {
    margin: auto;
}


body{
    display: flex;
    flex-direction:column;
    justify-content:center;
    min-height:100vh;
    background-image: url(image/bg.jpg);
    font-family: Courier, monospace;
    font-size: 28px;
    width: 50%;
    margin: 0 auto;
    text-decoration-color: #800000;
}

a {
    color: #005792;
}
.sourceCode{
    font-size: 16px;
}
```
:::
