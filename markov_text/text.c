#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define M 100
#define N 1001

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int seed = 0;
int pickRandomIndex(int max) {
    seed += 7;
    return seed % max;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void tabIntoString(char tab[][N], int n, char export[]){
    int i = 0;
    sprintf(export, "%s", tab[0]);
    for (i = 1; i < n; ++i) {
        strcat(export, " ");
        strcat(export, tab[i]); 
        //sprintf(export, "%s %s", export, tab[i]);
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int stringIntoTab(char string[], char export[][N]){
    int i ;
    int i_export = 0 , i_word = 0;
    int len = strlen(string) + 1;
    char word[M] ;

    for (i = 0 ; i < len ; ++ i){
        if (string[i] == ' ' || string[i] == '\0'){
            strncpy(export[i_export], word, strlen(word) + 1);
            word[0] = '\0';
            i_word = 0;
            ++i_export;
        }
        else
        {
            word[i_word] = string[i];
            word[i_word + 1] = '\0';
            ++i_word;
        }
    }
    return i_export;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void reducedTab(char tab[][N], int n, int nbr_kept){
    int i ,j;
    j = 0;
    for (i = n - nbr_kept; i < n ; ++i){
        strcpy(tab[j],tab[i]);
        ++j;
    }
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int indexNextWord(char words[][N], int nbr_words, char keys[][N], int nbr_keys){

    int i , j;
    int eventual[M];
    int i_eventual = 0;
    char key[N];
    int random_index;

    tabIntoString(keys, nbr_keys, key);

    for(i = nbr_keys - 1; i < nbr_words; ++ i){
        char test[M] = "";
        strcpy(test,words[i-(nbr_keys-1)]);
        for(j = i+1-(nbr_keys - 1) ; j < i+1 ; ++j){
            strcat(test, " ");
            strcat(test, words[j]);
            //sprintf(test, "%s %s", test, words[j]);
        }
        if (strcmp(key, test) == 0){
            eventual[i_eventual] = i + 1;
            sprintf(test, " ");
            ++i_eventual;
        }
    }
    if(i_eventual != 0){
        random_index = pickRandomIndex(i_eventual);
    }else{
        fprintf(stderr, "%s\n", key);
    }
    return eventual[random_index];

}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void addWordTab(char tab[][N], int n, char add[]){
    int i;
    for (i = 1; i < n; ++i){
        strcpy(tab[i - 1], tab[i]);
    }
    strcpy(tab[n-1], add);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int main()
{
    char t[1001];
    scanf("%[^\n]", t);
    int d;
    scanf("%d", &d);
    int l;
    scanf("%d", &l); fgetc(stdin);
    char s[1001];
    scanf("%[^\n]", s);

    char words[M][N];
    int nbr_words = stringIntoTab(t, words);
    int i ;
    char start[M][N];
    int nbr_start = stringIntoTab(s, start);
    int nbr_add = l-nbr_start;
    reducedTab(start, nbr_start, d);
    nbr_start = d;
    int i_next;
    char output[N];
    strcpy(output, s);



    for(i = 0; i < nbr_add ; ++i ){
        i_next = indexNextWord(words, nbr_words, start, nbr_start);
        strcat(output, " ");
        strcat(output, words[i_next]);
        //sprintf(output, "%s %s", output, words[i_next]);
        addWordTab(start, nbr_start, words[i_next]);
    }
    printf("%s\n", output);
    return 0;
}
