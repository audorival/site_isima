#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/random.h>
#include <math.h>


#define M 32
#define NBR_TEST 696969

int seed = 64;

int direction[4][2] = { {1, 0}, {0, 1}, {-1, 0}, {0, -1} };

int choiceRandomDir() {
    seed += 7;
    return random() % 4;
}


// effectue les deplacement de la fourmi et renvois le nombre de deplacement
int processAnt(char tab[][M], int w, int h, int pos[], int count, int steps){
    if (pos[0] >= h-1 || pos[1] >= w-1 || pos[0] <= 0 || pos[1] <= 0){
        return count;
    }else{

        int iDir = choiceRandomDir();
        pos[0] += direction[iDir][0] * steps;
        pos[1] += direction[iDir][1] * steps;
        ++count;
        return processAnt(tab, w, h, pos, count, steps);
    }
}

int main()
{
    srandom(seed);
    int steps; // longueur des pas ( [1;3])
    scanf("%d", &steps);
    int w; // largeur
    int h; // longueur
    int x;
    int y;
    int pos[2]; // position Ant
    char tab[M][M];
    int nbr_global_steps = 0;

    scanf("%d%d", &w, &h); fgetc(stdin);
    for (int i = 0; i < h; i++) {
        scanf("%[^\n]", tab[i]); fgetc(stdin);
        for (int j = 0; j < w; ++j){
            if (tab[i][j] == 'A'){
                x = i;
                y = j;
                tab[i][j] = '.';
            }
        }
    }


    for (int i = 0 ; i < NBR_TEST ; ++i){
        pos[0] = x;
        pos[1] = y;
        nbr_global_steps += processAnt(tab, w, h, pos, 0, steps);
    }

    float res = (float)nbr_global_steps/ NBR_TEST;

    printf("%0.1f", res);

    return 0;
}
