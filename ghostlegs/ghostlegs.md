# Résolution Ghost legs


Aubin Dorival Prép'isima 1 2023/2024

![Illustration GhostLeg](https://gitlab.isima.fr/audorival/markdownfichiers/-/raw/master/ghostlegs.png)
![UCA logo](https://gitlab.isima.fr/audorival/markdownfichiers/-/raw/master/UCA.jpg)

## Le problème

### Énoncé
>Ghost Legs est une sorte de jeu de loterie commun en Asie. Il commence par un certain nombre de lignes verticales. Entre les lignes, il y a des connecteurs horizontaux aléatoires liant toutes les lignes dans un diagramme connecté, comme celui ci-dessous.
```
A  B  C
|  |  |
|--|  |
|  |--|
|  |--|
|  |  |
1  2  3
```
>Pour jouer le jeu, un joueur choisit une ligne en haut et la suit vers le bas. Lorsqu’un connecteur horizontal est rencontré, il doit suivre le connecteur pour tourner vers une autre ligne verticale et continuer vers le bas. Répétez >cette opération jusqu’au bas du diagramme.
>
>Dans le diagramme de l’exemple, lorsque vous commencez à partir de A, vous vous retrouverez en 2. Partir de B se terminera en 1. Partir de C se terminera en 3. Il est garanti que chaque étiquette supérieure correspondra à une >étiquette inférieure unique.
>
>Avec un diagramme Ghost Legs, déterminez quelle étiquette supérieure est connectée à quelle étiquette inférieure. Énumérez toutes les paires connectées.

### Le problème

Le problème posé par l'éxercice est de trouver le chemin effectué par chaque colomne en entrée et
de trouvé leur colonne de sortie.

## Ma Solution

### Méthode

Les lignes sont ajouté par l'utilisateur et à chaque ligne nous allons regarder si
les entrées qui sont dans cette colonne doivent changer de colonne ou non.

### Algorithme

Ma logique est de vérifier a chaque colonne s'il y a des chemins à droite ou à gauche pour pouvoir changer de colonne. Pour les carractères de colonne on fais un cas particulier où on prend
la première ligne (`labels1`) et la dernière ligne (`labels2`). Pour les autres, on a une liste des indices qui permet de savoir où se situe les différentes entrées. Je regarde leurs positions
grâce à la valeur de `index` puis je regarde autour d'eux s'il y a le carractère  '-'. Si on a le carractère à gauche alors on soustrait 1 à l'indice, si c'est à droite on ajoute 1 et si il y
a rien à droite et à gauche alors on fait rien. Puis pour finir on affiche les solutions en prenant dans l'ordre  les premiers carractère puis on prend les derniers carractères avec l'indice
present dans `index` et avec le tableau `labels2`.

### Mon code 
```c

int main()
{
    int W; // taille largeur
    int H; // taille hauteur
    scanf("%d%d", &W, &H); fgetc(stdin);
    int nbr_colomn = W/3 +1;
    char labels1[nbr_colomn]; // liste carractere premiere ligne
    char labels2[nbr_colomn]; // liste carractere derniere ligne
    int index[nbr_colomn]; // indice des entrés 

    // initialisation des indices
    for (int a = 0; a < nbr_colomn; a++){
        index[a] = a ;
    }
    

    
    for (int i = 0; i < H; i++) {
        char line[1025];
        scanf("%[^\n]", line); fgetc(stdin);

        if(i == 0){ // Carracteres de la pemiere ligne
            for (int x = 0; x < W; x += 3){
                labels1[x/3] = line[x];
            }
        }else if ( i == H-1 ){ // carracteres de la derniere ligne
            for (int y = 0; y < W; y += 3){
                labels2[y/3] = line[y];
            }
        }else{
            int k = 0;
            for (int z = 0; z < nbr_colomn; z++ ){ // boucle regardant chaque indice ou ils sont 
                k = index[z] * 3;

                if (line[k - 1] == '-'){
                    index[z] --; // cas ou barre a gauche
                }else if (line[k + 1] == '-'){
                    index[z] ++; // cas ou barre a droite
                }
            }
        }
    }

    for(int f = 0; f < nbr_colomn; f ++){ // affichage des chemins 
        printf("%c%c\n", labels1[f], labels2[index[f]]);
    }
    

    return 0;
}

```

## Autre solution 

### La logique
La logiques trouvé sur codeingame proposais par un utilisateur est differentes de la mienne. En effet, il a opté de faire une matrice `map` pour faire la recherche après avoir rentré
les valeurs. La solution se base sur un changement des derniers carracteres qui sont à la derniere ligne de la matrice , quand il vois un '-' il effectut une permutation entre deux carractères
de cette dernière ligne. Puis pour finir il les affiches avec un pat de 3 pour tomber sur les carractères voulus. La logique ma plus car c'étais ma premiere idée que j'ais au final écarté car 
je pensais ne pas avoir certaine compétence. Je trouve également ingenieux d'utilisé le tableau pour garder les réponces.

### Code
```c
int main()
{
    int W,H;
    scanf("%d%d", &W, &H); fgetc(stdin);
    
    char map[101][101];
    for(int h=0 ; h<H ; h++ ){
        fgets(map[h], 101, stdin);
    }
    
    
    
    for(int h=H-2 ; h>0 ; h-- ){
        for(int w=1 ; w<W ; w+=3 ){
            if( map[h][w]=='-' ){
                char temp=map[H-1][w-1];
                map[H-1][w-1]=map[H-1][w+2];
                map[H-1][w+2]=temp;
            }
        }
    }


    for(int w=0 ; w<W ; w+=3 ){
        printf("%c%c\n",map[0][w],map[H-1][w]);
   } 
    return 0;
}
```



## Bilan apprentissage

- Les listes : j'ai apris la syntaxe et comment les manipulés.
- la gestion des carractères.
- j'ai aussi vu une nouvelle façon de faire mon algorithme avec la liste des indices `indexe`
