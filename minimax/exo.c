/* [[file:minimax.org::*Code][Code:1]] */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/**
* \file exo.c
* \authot Dorival.A
* \date 1 avril 2024
*
* Programe MinMax & ALphaBeta.
*
*/
/* Code:1 ends here */

/* [[file:minimax.org::*Code][Code:2]] */
/**
** \brief Structure node
**
** Cette strucure permet de representé un noeud dans l'arbre
** avec une valeur et un tableau avec tout les prochain noeud.
** Ainsi qu'une valeur n pour la taille du tableau et un bool
** pour savoir si c'est une feuille.
*/

typedef struct node {
    int val;
    struct node *tab[50];
    int n;
    bool leaf;
} node;
/* Code:2 ends here */

/* [[file:minimax.org::*Code][Code:3]] */
/**
* \brief Puissance
*
* Cette fonction permet de faire la puissance entre un nombre et une puisssance en entré.
* \return int n^p
*/
int power(int n, int p)
{
    int i;
    int res = n;
    for ( i = 0; i < p - 1; ++i )
    {
        res *= n;
    }
    return res;
}
/* Code:3 ends here */

/* [[file:minimax.org::*Code][Code:4]] */
/**
 * \brief Fonction max
 *
 * La fonction renvoie la plus grande valeur entre deux valeur entré en paramètre.
 *
 * */
int max(int a, int b)
{
    if ( a >= b )
    {
        return a;
    }
    else
    {
        return b;
    }
}

/**
 * \brief Fonction min
 *
 * La fonction renvoie la plus petite valeur entre dex valeurs entré en paramètre.
 *
 * */
int min(int a, int b)
{
    if ( a <= b )
    {
        return a;
    }
    else {
        return b;
    }
}
/* Code:4 ends here */

/* [[file:minimax.org::*Code][Code:5]] */
/**
* \brief Ligne de nombre en tableau de nombre
*
* Cette fonction permet de remplire un tableau de nombre
* grâce à la ligne string remplie de nombre séparer par
* des espace.
*
* \return void
*
*/
void lineIntoTable(char line[], int tab[], int nbr_leafs, int nbr_depth)
{
    int i;
    int k = 0, q = 0;
    int max_nbr = power(nbr_leafs, nbr_depth);
    int count = 0;
    char tmp[5];

    for ( i = 0; i < 10000; ++i)
    {
        if (line[i] != ' ' && line[i] != '\0')
        {
            tmp[k] = line[i];
            ++k;
        }
        else
        {
            tmp[k] = '\0';
            tab[q] = atoi(tmp);
            k = 0;
            ++q;
            ++count;
            if (count >= max_nbr)
            {
                return;
            }
        }
    }


}
/* Code:5 ends here */

/* [[file:minimax.org::*Code][Code:6]] */
/**
 *\brief Fonction créant l'arbre des noeuds
 *
 *Cette fonction fais l'arboraissance des noeuds jusqu'au
 *feuille pour représenter le min et max du jeu.
 * */
void makeTree(node *origin, int depth, int n, node *leaf[], int *k)
{
    int i;
    origin->n = n;
    origin->leaf = false;
    for (i = 0; i < n; ++i)
    {
        origin->tab[i] = malloc(sizeof(node));
        if (depth == 0)
        {
            leaf[*k] = origin->tab[i];
            leaf[*k]->leaf = true;
            leaf[*k]->n = 0;
            ++(*k);
        }else
        {

            makeTree(origin->tab[i], depth - 1, n, leaf, k);
        }
    }
}
/* Code:6 ends here */

/* [[file:minimax.org::*Code][Code:7]] */
void freeNode(node* root) {
    if (root == NULL) {
        return;
    }
    int maxi = root->n;
    for (int i = 0; i < maxi; ++i) {
        freeNode(root->tab[i]);
    }

    free(root);
}
/* Code:7 ends here */

/* [[file:minimax.org::*Code][Code:8]] */
/**
 *\brief Remplit les feuilles
 *
 *La fonction trouve et remplit les noeuds feuilles avec leur valeurs
 * */

void addValLeafs(node *leafs[], int n, int val[])
{
    int i;

    for ( i = 0; i < n; ++i )
    {
        leafs[i]->val = val[i];
    }
}
/* Code:8 ends here */

/* [[file:minimax.org::*Code][Code:9]] */
/**
* \brief Fonction AlphaBeta
*
* Cette fonction aplique l'algorithme alpha beta, en effet elle permet
*d'utiliser le minmax en ne parcourant pas
* certaine branche qui ne rajoute aucune information.
*/
int alphabeta(node parent, int depth, int a, int b, bool maximizing, int *count )
{
    ++(*count);
    if ( depth <= 0 || parent.leaf == true)
    {
        return parent.val;
    }

    int i;
    int val;

    if (maximizing)
    {
        val = -1000;
        for (i = 0; i < parent.n; ++i)
        {
            val = max(val, alphabeta(*parent.tab[i], depth - 1, a, b, false, count));
            if (val >= b)
            {
                break;
            }
            a = max(a, val);
        }
        return val;

    }else
    {
        val = 1000;
        for (i = 0; i < parent.n; ++i)
        {
            val = min(val, alphabeta(*parent.tab[i], depth - 1, a, b, true, count));
            if ( val <= a )
            {
                break;
            }
            b = min(b, val);
        }
        return val;
    }
}
/* Code:9 ends here */

/* [[file:minimax.org::*Code][Code:10]] */
/**
* \brief Main
*
* Le main permet de prendre D, B, et les fueilles de l'arbre.
* Puis utilise les differente fonction pour trouver la valeur MinMax.
*
*
*/
int main()
{
    int D;
    int B;
    scanf("%d%d", &D, &B); fgetc(stdin);
    char LEAFS[400001];
    scanf("%[^\n]", LEAFS);
    int tab[10000];
    int nbr = power(B, D);
    node *leaf[10000];
    int k = 0;
    // Write an answer using printf(). DON'T FORGET THE TRAILING \n
    // To debug: fprintf(stderr, "Debug messages...\n");

    lineIntoTable(LEAFS, tab, B , D);
    node *origin = malloc(sizeof(node) * 1);
    int count = 0;
    makeTree(origin, D - 1, B, leaf, &k);
    //printf("%p\n", leaf[0]);
    addValLeafs(leaf, nbr, tab);


     
    int rep = alphabeta(*origin, D, -1000, 1000, true, &count);
    printf("%d %d\n", rep, count);

    freeNode(origin);
    return 0;
}
/* Code:10 ends here */
