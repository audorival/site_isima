#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define N_DIR_BOMB 4
#define FALSE 0
#define TRUE 1

int inTab(char tab[], int n, char search){
    int i;
    int find = FALSE;

    for(i = 0; i < n; ++i){
        if (tab[i] == search){
            find = TRUE;
        }
    }
    return find;
}

int main()
{
    // width of the building.
    int W;
    // height of the building.
    int H;
    scanf("%d%d", &W, &H);
    // maximum number of turns before game over.
    int N;
    scanf("%d", &N);
    int X0;
    int Y0;
    scanf("%d%d", &X0, &Y0);

    int x = X0;
    int y = Y0;
    int min_x = 0;
    int max_x = W-1;
    int min_y = 0;
    int max_y = H-1;



    // game loop
    while (1) {
        // the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
        char bomb_dir[4];
        scanf("%s", bomb_dir);

        if( inTab(bomb_dir, N_DIR_BOMB, 'U') )
        {
            max_y = y;
        }
        else if( inTab(bomb_dir, N_DIR_BOMB, 'D') )
        {
            min_y = y + 1;

        }

        if( inTab(bomb_dir, N_DIR_BOMB, 'R') )
        {
            min_x = x + 1;

        }
        else if( inTab(bomb_dir, N_DIR_BOMB, 'L') )
        {
            max_x = x;
        }

        // dichotomie :
        x = ( max_x + min_x ) / 2 ;
        y = ( max_y + min_y ) / 2 ;

        // the location of the next window Batman should jump to.
        printf("%d %d\n", x, y);
    }

    return 0;
}
