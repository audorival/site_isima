/* [[file:dwarf.org::*Include][Include:1]] */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 10000
/* Include:1 ends here */

/* [[file:dwarf.org::maxIntTab][maxIntTab]] */
int maxIntTab(int tab[], int n)
{
    int i;
    int max = tab[0];

    for (i = 1; i < n ; ++i)
    {
        if (tab[i] > max)
        {
            max = tab[i];
        }
    }
    return max;
}
/* maxIntTab ends here */

/* [[file:dwarf.org::countMaxLength][countMaxLength]] */
int countMaxLenght(int tab[][2], int n, int act)
{
    int all_result[100];
    int k = -1; //indexe of all_result
    int i;

    for ( i = 0; i < n; ++i)
    {
        if (tab[i][0] == act)
        {
            ++k;
            all_result[k] = countMaxLenght(tab, n, tab[i][1]) + 1;
        }
    }

    if (k == -1)
    {
        return 0;
    }

    return (k == -1)? 0 : maxIntTab(all_result, k + 1);

}
/* countMaxLength ends here */

/* [[file:dwarf.org::main][main]] */
int main()
{
    // the number of relationships of influence
    int n;
    scanf("%d", &n);
    int influences_tab[N][2];
    for (int i = 0; i < n; i++) {
        // a relationship of influence between two people (x influences y)
        scanf("%d%d", &influences_tab[i][0], &influences_tab[i][1]);
    }


    // The number of people involved in the longest succession of influences
    //printf("2\n");
    int i;
    int possible[N];
    for ( i = 0; i < n; ++i )
    {
        possible[i] = countMaxLenght(influences_tab, n, influences_tab[i][0]);
    }


    printf("%d\n", maxIntTab(possible, n) + 1);
    return 0;
}
/* main ends here */
