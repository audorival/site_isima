/* [[file:sudoku.org::*Include][Include:1]] */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
/* Include:1 ends here */

/* [[file:sudoku.org::*inLine][inLine:1]] */
bool inLine(char tab[][10], char val, int line)
{
    int i;
    bool in = false;

    for ( i = 0; i < 9; ++i )
    {
        if ( tab[line][i] == val )
        {
            in = true;
        }
    }
    return in;
}
/* inLine:1 ends here */

/* [[file:sudoku.org::*inColumn][inColumn:1]] */
bool inColumn(char tab[][10], char val, int column)
{
    int i;
    bool in = false;
    for ( i = 0; i < 9; ++i )
    {
        if ( tab[i][column] == val )
        {
            in = true;
        }
    }

    return in;
}
/* inColumn:1 ends here */

/* [[file:sudoku.org::*inBox][inBox:1]] */
bool inBox(char tab[][10], char val, int line, int column)
{
    int indexeBoxLine = line / 3;
    int indexeBoxColumn = column / 3;

    int lineTest = indexeBoxLine * 3;
    int columnTest = indexeBoxColumn *3;

    int i, j;
    bool in = false;

    for ( i = 0; i < 3; ++i )
    {
        for ( j = 0; j < 3; ++j )
        {
            if ( tab[lineTest + i][columnTest + j] == val )
            {
                //printf("i=%d, j=%d\n", i, j);
                in = true;
            }
        }
    }

    return in;
}
/* inBox:1 ends here */

/* [[file:sudoku.org::*nbrCage][nbrCage:1]] */
int nbrCage(char cages[], char key)
{
    int i;
    char str_nbr[4] = "";
    int k = 0;
    for ( i = 0; i < 256; ++i )
    {
        if (cages[i] == key)
        {
            i += 2;
            while ( cages[i] != ' ' && cages[i] != '\0' )
            {
                str_nbr[k] = cages[i];
                ++i;
                ++k;
            }
            str_nbr[k] = '\0';
            //printf("%s, atoi = %d\n", str_nbr, atoi(str_nbr));
            return atoi(str_nbr);
        }
    }
    return -1;
}
/* nbrCage:1 ends here */

/* [[file:sudoku.org::*nbrCage][nbrCage:2]] */
int nbrCase(char tab[][10], char val)
{
    int i, j;
    int count = 0;
    for ( i = 0; i < 9; ++i )
    {
        for ( j = 0; j < 9; ++j )
        {
            if (tab[i][j] == val )
            {
                ++count;
            }
        }
    }
    return count;
}
/* nbrCage:2 ends here */

/* [[file:sudoku.org::*nbrCage][nbrCage:3]] */
bool goodInCage(char tab_nbr[][10], char tab_cages[][10], char cages[], char val, int line, int column)
{
    int i, j;
    char name_cage = tab_cages[line][column];
    int nbr_max = nbrCage(cages, name_cage);
    int nbr_case_cage = nbrCase(tab_cages, name_cage);
    int count = 0;
    int nbr_filled= 0;
    char temp[4] = "";
    for ( i = 0; i < 9; ++i )
    {
        for ( j = 0; j < 9; ++j )
        {
            //puts("here");
            if (tab_cages[i][j] == name_cage)
            {
                //temp = tab_nbr[i][j];
                sprintf(temp, "%c", tab_nbr[i][j]);
                //printf("avant : %s ,atoi %d\n",  temp, atoi(&temp[0]));
                count += atoi(&temp[0]);
                if ( tab_nbr[i][j] != '.' )
                {
                    ++nbr_filled;
                }
                if ( count > nbr_max || tab_nbr[i][j] == val )
                {
                    //printf("Pas valid - val=%c x=%d y=%d\n", val, i, j);
                    //printf("count = %d, max : %d", count, nbr_max);
                    return false;
                }
            }
        }
    }
    count += atoi(&val);
    //printf("nbr_case_cage : %d , nbr_filled : %d\n", nbr_case_cage, nbr_filled);
    //printf("max = %d, count = %d\n", nbr_max, count);

    if ( nbr_case_cage == nbr_filled + 1 && count != nbr_max )
    {
        //puts("Faux !\n");
        return false;
    }
    return true;


}
/* nbrCage:3 ends here */

bool findEmpty(char tab[][10])
{
    int i, j;
    for ( i = 0; i < 9; ++i )
    {
        for ( j = 0; j < 9; ++j )
        {
            if (tab[i][j] == '.')
            {
                return true;
            }
        }
    }
    return false;
}

/* [[file:sudoku.org::*nbrCage][nbrCage:5]] */
void find0(char tab[][10], int coor[])
{
    int i, j;

    for (i = 0; i < 9; ++i)
    {
        for ( j = 0; j < 9; ++j )
        {
            if (tab[i][j] == '.')
            {
                coor[0] = i;
                coor[1] = j;
                return;
            }
        }
    }
}
/* nbrCage:5 ends here */

/* [[file:sudoku.org::*nbrCage][nbrCage:6]] */
int valid(char tab[][10],char tab_cages[][10], char cages[], int line, int column, char val)
{
    //printf("valid line %d , true = %d\n", goodInCage(tab, tab_cages, cages, val, line, column), true);
    if ( !inLine(tab, val, line)
         && !inColumn(tab, val, column)
         && !inBox(tab, val, line, column)
         && goodInCage(tab, tab_cages, cages, val, line, column))
    {
        //puts("Valid !");
        return true;
    }

    return false;
}
/* nbrCage:6 ends here */

/* [[file:sudoku.org::*nbrCage][nbrCage:7]] */
bool search(char tab_nbr[][10], char tab_cages[][10], char cages[] )
{
    int i;
    int coor[2];
    find0(tab_nbr, coor);
    int x = 0, y = 0;
    x = coor[0];
    y = coor[1];
    //printf("x%d y%d\n",x, y );

    if (!findEmpty(tab_nbr))
    {
        return true;
    }



    for ( i = 1; i < 10; ++i )
    {
        //printf(" test de %c\n", i+'0');
        if (valid(tab_nbr, tab_cages, cages, x, y, i + '0'))
        {
            tab_nbr[x][y] = '0' + i;
            //printf("%c\n", tab_nbr[x][y]);

            if (search(tab_nbr, tab_cages, cages))
            {
                return true;
            }

            tab_nbr[x][y] = '.';
        }
    }
    return false;

}
/* nbrCage:7 ends here */

/* [[file:sudoku.org::*Main][Main:1]] */
int main()
{
    char tab_nbr[9][10];
    char tab_cages[9][10];
    for (int i = 0; i < 9; i++) {
        scanf("%s%s", tab_nbr[i], tab_cages[i]); fgetc(stdin);
    }
    char cages[251];
    scanf("%[^\n]", cages);

    // Write an answer using printf(). DON'T FORGET THE TRAILING \n
    // To debug: fprintf(stderr, "Debug messages...\n");
    //printf("solution\n");
    //puts("~~~~~~~~~~~~~~~~~~~~~~~~");
    search(tab_nbr, tab_cages, cages);

    int i;
    for (i = 0; i < 9; ++i) {
        printf("%s\n", tab_nbr[i]);
    }


    //printf("%d %d\n", !findEmpty(tab_nbr), true);


    return 0;
}
/* Main:1 ends here */
