/* [[file:sudoku.org::*Inlude][Inlude:1]] */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
/* Inlude:1 ends here */


/**
 * \file solver.c
 * \author Dorival.A
 * \date 28 mars 2024
 *
 * Programe pour resoudre un soduku.
 */


/* [[file:sudoku.org::*inLine][inLine:1]] */

/**
* \brief Fonction inLine
*
* Cette fonction permet de savoir si <val> est dans la
* ligne du tableau.
*
* \return <true> si dans la ligne , <false> si il est pas dans la ligne
*
*/
bool inLine(char tab[][10], char val, int line)
{
    int i;
    bool in = false;

    for ( i = 0; i < 9; ++i )
    {
        if ( tab[line][i] == val )
        {
            in = true;
        }
    }
    return in;
}
/* inLine:1 ends here */

/* [[file:sudoku.org::*inColumn][inColumn:1]] */
bool inColumn(char tab[][10], char val, int column)
{
    int i;
    bool in = false;
    for ( i = 0; i < 9; ++i )
    {
        if ( tab[i][column] == val )
        {
            in = true;
        }
    }

    return in;
}
/* inColumn:1 ends here */

/* [[file:sudoku.org::*inBox][inBox:1]] */
bool inBox(char tab[][10], char val, int line, int columne)
{
    int indexeBoxLine = line / 3;
    int indexeBoxColumn = columne / 3;

    int lineTest = indexeBoxLine * 3;
    int columnTest = indexeBoxColumn *3;

    int i, j;
    bool in = false;
    
    for ( i = 0; i < 3; ++i )
    {
        for ( j = 0; j < 3; ++j )
        {
            if ( tab[lineTest + i][columnTest + j] == val )
            {
                in = true;
            }
        }
    }
     
    return in;
}
/* inBox:1 ends here */

bool findEmpty(char tab[][10])
{
    int i, j;
    for ( i = 0; i < 9; ++i )
    {
        for ( j = 0; j < 9; ++j )
        {
            if (tab[i][j] == '0')
            {
                return true;
            }
        }
    }
    return false;
}

/* [[file:sudoku.org::*valid][valid:1]] */
int valid(char tab[][10], int line, int column, char val)
{

    if ( !inLine(tab, val, line) && !inColumn(tab, val, column) && !inBox(tab, val, line, column))
    {
        return true;
    }

    return false;
}
/* valid:1 ends here */

/* [[file:sudoku.org::*find0][find0:1]] */
void find0(char tab[][10], int coor[])
{
    int i, j;

    for (i = 0; i < 9; ++i)
    {
        for ( j = 0; j < 9; ++j )
        {
            if (tab[i][j] == '0')
            {
                coor[0] = i;
                coor[1] = j;
                return ;
            }
        }
    }
}
/* find0:1 ends here */

/* [[file:sudoku.org::*search][search:1]] */
bool search(char tab[][10])
{

    if (!findEmpty(tab))
    {
        return true;
    }


    int i;
    int coor[2];
    find0(tab, coor);
    int x = coor[0];
    int y = coor[1];
    
    for ( i = 1; i < 10; ++i )
    {
        if (valid(tab, x, y, i + '0'))
        {
            tab[x][y] = '0' + i;

            if (search(tab))
            {
                return true;
            }

            tab[x][y] = '0';
        }
    }
    return false;

}
/* search:1 ends here */

/* [[file:sudoku.org::*Main][Main:1]] */
int main()
{
    char tab[9][10];
    for (int i = 0; i < 9; i++) {
        scanf("%[^\n]", tab[i]); fgetc(stdin);
    }

    // Write an answer using printf(). DON'T FORGET THE TRAILING \n
    // To debug: fprintf(stderr, "Debug messages...\n");
    //printf("answer\n");
    int i;
    search(tab);

    for ( i = 0; i < 9; ++i )
    {
        printf("%s\n", tab[i]);
    }


    return 0;
}
/* Main:1 ends here */
